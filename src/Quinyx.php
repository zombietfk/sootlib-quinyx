<?php

namespace Sootlib\Quinyx;

use Sootlib\WSDL\WSDLDefinedWebService;
use DateTime;
use DateInterval;

class Quinyx {

    public $keys;
    private $ws;

    public static function getKeysForAllUnits(string $a_key, WSDLDefinedWebService $using_ws, bool $verbose = false){
        $out = json_encode($using_ws->wsdlGetUnitsAPIKey($a_key));
        $out = json_decode($out, true);
        $keys = array();
        if(!$verbose){
            foreach($out as $key){
                $keys[] = $key["API_key"];
            }
            return $keys;
        } else {
          return $out;
        }
    }
    
    
    public function getSalaryPeriodLockInfo(){
        $result = $this->doForAllKeys($this->keys,"wsdlGetSalaryPeriodLockStatus", array());
        return $result;
    }

    public function __construct(array $keys, WSDLDefinedWebService $ws) {
        $this->keys = $keys;
        $this->ws = $ws;
    }

    public function updateQuinyxForecasts($forcast_data,$for_variable_id){
        $out = array();
        foreach($forcast_data as $api_key => $date) {
            foreach($date as $date_value => $entity_set) {
                $proc_entities = array();
                foreach($entity_set as $entity){
                    array_push($proc_entities, array(
                        "time"=>$entity["hour"],
                        "amount"=>$entity["value"]
                    ));
                }
                $nout = $this->ws->wsdlUpdateForecastsV3(
                    $api_key,
                    1,
                    array(
                        array(
                            "date" => $date_value,
                            "variableTypeId" => $for_variable_id,
                            "periods" => $proc_entities,
                            "validationErrors" => array()
                        )
                    )
                );
                array_push($out, $nout);
            }
        }
        return $out;
    }

    public function getAbsentShifts(DateTime $from, DateTime $to){
        $key = $this->keys[0];
        $result = $this->doForSingleKey($key, "wsdlGetSchedulesV2", array(
            "fromDate"=> $from->format("Y-m-d"),
            "toDate"=> $to->format("Y-m-d"),
            "allUnits"=> true,
            "absenceShifts"=> true,
            "includeCosts" => true,
            "includeNOI" => true
        ));
        $out = array();
        foreach($result->schedules as $r){
            if(isset($r->orgScheduleId) && isset($r->absenceScheduleId)){
                array_push($out, $r);
            }
        }
        return $out;
    }

    public function getShifts(DateTime $from, DateTime $to){
        $key = $this->keys[0];
        $result = $this->doForSingleKey($key, "wsdlGetSchedulesV2", array(
            "fromDate"=> $from->format("Y-m-d"),
            "toDate"=> $to->format("Y-m-d"),
            "allUnits"=> true,
            "includeCosts" => true
        ));
        return $result->schedules;
    }
        
        public function getAgreements(DateTime $from, DateTime $to){
        $localAgreements = array();
        foreach($this->keys as $key){
            $result = $this->doForSingleKey($key, "wsdlGetAgreementsV2", array(
                "fromDate"=> $from->format("Y-m-d"),
                "toDate"=> $to->format("Y-m-d")
            ));
            if(count($result->agreements) > 0) {
                foreach($result->agreements as $ag){
                    $localAgreements[] = $ag;
                }
            }
        }
        return $localAgreements;
    }
        
    public function getAgreementTemplates(){
        $localAgreements = array();
        foreach($this->keys as $key){
            $result = $this->doForSingleKey($key, "wsdlGetAgreementTemplatesV2", array());
                if(count($result->agreementTemplates) > 0) {
                     foreach($result->agreementTemplates as $ag){
                         $localAgreements[] = $ag;
                     }
                }
            }
        return $localAgreements;
    }
        
    public function getPayroll(DateTime $from, DateTime $to){
        $result = $this->doForAllKeys($this->keys, "wsdlGetPayrollV2", array(
            "fromDate"=> $from->format("Y-m-d"),
            "toDate"=> $to->format("Y-m-d"),
            "attest" => false,
            "pSalType" => "0,1,2,3",
            "empInUnitOnly" => false,
            "employeeId" => "",
            "badgeNo" => "",
            "includeCosts" => true
        ));
        return $result;
    }

    public function getRoles(){
        $key = $this->keys[0];
        $result = $this->doForSingleKey($key, "wsdlGetRoles", array());
        return $result;
    }

    public function getEmployees(){
        $result = $this->doForAllKeys($this->keys, "wsdlGetEmployees", array());
        return $result;
    }

    public function getEmployeeGroups($group_id = 0){
        $out = $this->doForAllKeys($this->keys, "wsdlGetEmployeeGroups", array($group_id), true);
        return $out;
    }

    public function getSections(){
        $result = $this->doForAllKeys($this->keys, "wsdlGetSections", array(), true);
        return $result;
    }

    public function getEmployeeSkills(){
        //@TODO NEED TO FIGURE OUT ARGS FOR METHOD
        return $this->doForAllKeys($this->keys, "wsdlGetEmployeeSkills", array(), true);
    }
    
    public function getTimePunches(DateTime $from, DateTime $to){
      $data = array(
        "fromDate" => $from->format("Y-m-d"),
        "toDate" => $to->add(new DateInterval("PT24H"))->format("Y-m-d"),
        "approved" => true, //setting to false means that all shifts will be returned
        "unapproved" => true,
        "deleted" => true,
        "active" => true,
        "allUnits" => true
      );
      $key = $this->keys[0];
      $result = $this->doForSingleKey($key, "wsdlGetTimePunches", $data);
      return $result;
    }
    
    public function getShiftCategories(){
      $data = array(
        "lastModified"=>""
      );
      $result = $this->doForSingleKey($this->keys[0], "wsdlGetShiftCategories", array_values($data), true);
      return $result;
    }
    
    public function getCategories($categoryType){
      $data = array(
        "categoryType"=>$categoryType,
        "lastModified"=>""
      );
      $result = $this->doForSingleKey(
        $this->keys[0],
        "wsdlGetCategories",
        array_values($data),
        true
      );
      return $result;
    }
    
    private function doForAllKeys($keys, $wsdlAction, $wsdlArgs, $splatArgs = false){
        $aggregate = array();
        foreach($keys as $key){
            $out = $this->doForSingleKey($key, $wsdlAction, $wsdlArgs, $splatArgs);
            if(!is_array($out)){
              $out = array($out);
            }
            foreach($out as $e){
                $e = json_encode($e);
                $e = json_decode($e, true);
                if(!is_array($e)){
                    $e = array(
                        "value"=>$e
                    );
                }
                $e["api_key"] = $key;
                $e = json_encode($e);
                $e = json_decode($e);
                array_push($aggregate, $e);
            }
        }
        return $aggregate;
    }

    private function doForSingleKey($key, $wsdlAction, $wsdlArgs, $splatArgs = false){
        if($splatArgs){
            $result = $this->ws->$wsdlAction(
                $key,
                ...$wsdlArgs
            );
        } else {
            $result = $this->ws->$wsdlAction(
                $key,
                $wsdlArgs
            );
        }
        return $result;
    }
}
